import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthenticationService, public router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const tokens =
      this.auth._credentials !== undefined && this.auth._credentials !== null
        ? this.auth._credentials.token
        : 'loginToken';
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${tokens}`
      }
    });
    // return next.handle(request);

    return next.handle(request).pipe(catchError((error, caught) => {
      // intercept the respons error and displace it to the console
      console.log(error);
      this.handleAuthError(error);
      return throwError(new Error(error));
    }) as any);
  }

  /**
   * manage errors
   * @param err
   * @returns {any}
   */
  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    // handle your auth error or rethrow
    if (err.status === 401) {
      // navigate /delete cookies or whatever
      // console.log('handled error ' + err.status);
      /* this.snackBar.open('Oops, Token has expired, must to login again!', '', {
        duration: 6000,
      }); */
      this.router.navigate([`/login`]);
      return throwError(new Error(err.message));
    }
    throw err;
  }
}
