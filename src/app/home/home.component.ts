import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Todos } from '@app/models/todo.model';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { TodoService } from './services/todo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  quote: string;
  isLoading: boolean;
  todos: Array<Todos>;

  constructor(private todoService: TodoService, private router: Router,
    private route: ActivatedRoute) {
      this.listTodo();
    }

  ngOnInit() {
    this.isLoading = true;
  }

  listTodo() {
    this.todoService.todoList()
      .pipe()
      .subscribe((body: any) => {
        if (body.status === true && body.data.length > 0) {
          this.todos = body.data;
        }
      });
  }
  newTask() {
    this.router.navigate(['/todo-create'], { replaceUrl: true });
  }

  deleteTask(id: any) {
    this.todoService.todoDelete(id)
      .pipe()
      .subscribe((body: any) => {
        if (body.status === true) {
          this.listTodo();
        }
      });
  }
}
