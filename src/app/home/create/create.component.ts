import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { Todos } from '@app/models/todo.model';
import { TodoService } from '@app/home/services/todo.service';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Logger } from '@app/core';

const log = new Logger('User');
// const credentialsKey = 'credentials';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public todo: Todos;
  public editMode: boolean;

  public todoStatus = [
    { value: 'Archived', viewValue: 'Inactive' },
    // {value: 'Invited', viewValue: 'Invited'},
    { value: 'Active', viewValue: 'Active' }
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private todoService: TodoService,
    private authenticationService: AuthenticationService
  ) {
    this.todo = new Todos();
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      /* params.id is null, create new user */
      this.editMode = false;
    });
  }

  onSubmit() {
    this.create();
  }

  create() {
    this.todoService
      .todoCreate(this.todo)
      .pipe(
        finalize(() => {
        })
      )
      .subscribe(
        (data: any) => {
          if (data.status === true) {
            // Todo list created
            this.router.navigate(['/home'], { replaceUrl: true });
          } else {
            // todo list failed to create
            this.router.navigate(['/home'], { replaceUrl: true });
          }
        },
        error => {
          this.router.navigate(['/home'], { replaceUrl: true });
        }
      );
  }

  goBack() {
    this.router.navigate(['/home'], { replaceUrl: true });
  }
}
