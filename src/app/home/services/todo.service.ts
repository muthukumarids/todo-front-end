import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Todos } from '@app/models/todo.model';

/* routes API url defined */
const routes = {
  getAllTodos: `/todo`,
  postTodos: `/todo`,
  putTodos: `/todo`,
  deleteTodos: `/todo`
};

@Injectable()
export class TodoService {
  constructor(private http: HttpClient) {}

  /**
   *
   * @param filter - search box entry
   * @param sortDirection  - ascending or decending
   * @param sortColumn  - column
   * @param pageIndex - current page
   * @param pageSize - total number of records
   */
  todoList() {
    return this.http
      // .cache()
      .get(routes.getAllTodos, {
        params: new HttpParams()
        })
      .pipe(
        map((body: any) => {
          return body;
        }),
        catchError(() => of('Error, could not load todos :-('))
      );
  }

  /**
   *
   * @param todoId - getting the todo information
   */
  getTodo(todoId: any) {
    return this.http
      .cache()
      .get(routes.getAllTodos + '/' + todoId)
      .pipe(
        map((body: any) => {
          return body;
        }),
        catchError(() => of('Error, could not load Users :-('))
      );
  }
  /**
   *
   * @param todo - create new todo
   */
  todoCreate(todo: Todos) {
    return new Observable(observer => {
      this.http.post<Todos>(routes.postTodos, todo).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        data => {
          observer.next(data);
          observer.complete();
        },
        error => {
          observer.error(error);
        }
      );
    });
  }
  /**
   *
   * @param todo - - todo update
   */
  todoUpdate(todo: Todos) {
    return new Observable(observer => {
      this.http.put<Todos>(routes.putTodos + '/' + todo.id, todo).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        data => {
          observer.next(data);
          observer.complete();
        },
        error => {
          observer.error(error);
        }
      );
    });
  }

  /**
   *
   * @param todo - delete todo
   */
  todoDelete(todo: any) {
    return new Observable(observer => {
      this.http.delete<Todos>(routes.putTodos + '/' + todo).subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        data => {
          observer.next(data);
          observer.complete();
        },
        error => {
          observer.error(error);
        }
      );
    });
  }
}
