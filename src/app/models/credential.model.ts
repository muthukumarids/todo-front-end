export class Credentials {
  // Customize received credentials here
  email: string;
  token: string;
  data: {
    role: string;
    email: string;
    userId: number;
    firstName: string;
    lastName: string;
    empID: number;
  };
}

export interface LoginContext {
  email: string;
  password: string;
  remember?: boolean;
}
