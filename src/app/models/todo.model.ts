export class Todos {
    // Customize received credentials here
    id: number;
    taskName: string;
    description: string;
    status: string;
    createdDate: string;
}

